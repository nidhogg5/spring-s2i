# spring-s2i
FROM gradle:jdk8

LABEL maintainer="Your Name <your@email.com>"

ENV BUILDER_VERSION 1.0

# TODO: Set labels used in OpenShift to describe the builder image
#LABEL io.k8s.description="Platform for building xyz" \
#      io.k8s.display-name="builder x.y.z" \
#      io.openshift.expose-services="8080:http" \
#      io.openshift.tags="builder,x.y.z,etc."

# TODO: Install required packages here:
# RUN yum install -y ... && yum clean all -y
# RUN yum install -y rubygems && yum clean all -y
# RUN gem install asdf

# TODO (optional): Copy the builder files into /opt/app-root
# COPY ./ /opt/app-root/

# TODO: Copy the S2I scripts to /usr/libexec/s2i, since openshift/base-centos7 image
LABEL io.openshift.s2i.scripts-url="image:///usr/libexec/s2i"
COPY ./s2i/bin/ /usr/libexec/s2i

# TODO: Drop the root user and make the content of /opt/app-root owned by user 1001
# RUN chown -R 1001:1001 /opt/app-root

# This default user is created in the openshift/base-centos7 image
USER 1000

# TODO: Set the default port for applications built using this image
EXPOSE 9000

# TODO: Set the default CMD for the image
CMD ["/usr/libexec/s2i/usage"]
