package com.example.springs2i;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class SpringS2iApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringS2iApplication.class, args);
    }

    @GetMapping("")
    public String greeting() {
        return "Happy Hello, World2";
    }
}
